import React from 'react';
import './index.css';

const Movies = (props) => {
    return (
       <div id="container"> 
            <div className="image">
                <img alt="poster" src={props.image}/>
            </div>
            <div className="info">
                {props.name},
                sala: {props.hall},
                cena: {props.price}din
            </div>
        </div>
    );
};

export default Movies;