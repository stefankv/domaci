import React from 'react';
import { createRoot } from 'react-dom/client';
import './index.css';
import Movies from './Movies.js';
import Cinema from './Cinema.js';

const App = () => {
  const current = new Date();
  const date = `${current.getDate()}.${current.getMonth()+1}.${current.getFullYear()}.`;

  return <div>
    <h1>Repertoar za danas ({date})</h1>
    <Cinema>
      <Movies 
        image="capAm.jpeg"
        name="Captain America - The first avanger"
        hall="2"
        price="350"></Movies>
    </Cinema>
    <Cinema>
      <Movies
        image="papillon.jpeg"
        name="The papillon"
        hall="1"
        price="300"></Movies>
    </Cinema>
    <Cinema>
      <Movies
        image="lostCity.jpeg"
        name="The lost city of Z"
        hall="5"
        price="350"></Movies>
    </Cinema>
  </div>
}

const rootElement = document.getElementById('root');
const root = createRoot(rootElement);
root.render(
    <App/>
);
