import React from 'react';
import { createRoot } from 'react-dom/client';

const App = () => {
  const buttonText = 'Click';
  return <div>
    <label htmlFor="name">
      Enter name:
    </label>
    <input id="name" type="text"/>
    <button style={{backgroundColor: 'blue', color: 'white'}}>
      {buttonText}
    </button>
  </div>;
};

const rootElement = document.getElementById('root');
const root = createRoot(rootElement);

root.render(
    <App/>,
);