import React from 'react';
import { createRoot } from 'react-dom/client';
import './index.css';
import Movies from './Movies.js';

const App = () => {
  const current = new Date();
  const date = `${current.getDate()}.${current.getMonth()+1}.${current.getFullYear()}.`;

  return <div>
    <h1>Repertoar za danas ({date})</h1>
    <Movies 
      name="Captain America - The first avanger"
      hall="2"
      price="350"></Movies>
    <Movies
      name="The papillon"
      hall="1"
      price="300"></Movies>
    <Movies
      name="The lost city of Z"
      hall="5"
      price="350"></Movies>
  </div>
}

const rootElement = document.getElementById('root');
const root = createRoot(rootElement);
root.render(
    <App/>
);