import React from 'react';

const Cinema = props => {
    return (
        <div className="containerCinema">
            <div className="content">
                {props.children}
            </div>
            <div className="extra content">
                <div className="ui two buttons">
                    <button>Like</button>
                    <button>Dislike</button>
                </div>
            </div>
        </div>
    );
};
export default Cinema;